<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@page contentType = "text/html; charset=utf-8"%>
<%@page import="java.sql.*, javax.sql.*, java.io.*, java.net.*"%>
<!--
xml파싱을 위한 자바 라이브러리 임포트
 -->
<%@page import="javax.xml.parsers.*, org.w3c.dom.*"%>
<html>
<head></head><body>
<h1>wifi 조회</h1>
<%
	//파일에서 직접 파싱, DocumentBuilderFactory, builder 생성
	DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	//xml문서를 파싱 > 그 결과를 document 인스턴스에 저장
	Document doc = docBuilder.parse(new File("/var/lib/tomcat8/webapps/ROOT/ch02-xml/wifi.xml"));
	//해당 노드에 접근해서 필요 데이터 추출
	// Document의 최상위 element인 root node에 접근
	Element root = doc.getDocumentElement();
	//테그네임으로 웹페이지에서 특정요소 찾기
	NodeList tag_address = doc.getElementsByTagName("소재지지번주소");
	NodeList tag_lat = doc.getElementsByTagName("위도");
	NodeList tag_long = doc.getElementsByTagName("경도");
	
%>
	<table cellspacing=1 width=500 border=1>
	<tr>
	<td width=100>순번</td>
	<td width=100>소재지지번주소</td>
	<td width=100>위도</td>
	<td width=100>경도</td>
	</tr>


<%
	for (int i =0; i <tag_address.getLength(); i++) {
		out.println("<tr>");
		try {
			out.println("<td width=100>" + i + "</td>");
			//getFirstChild():element node에 접근
			//getNodeeValue(): 해당 node의 값 가져오는 메서드
			out.println("<td width=100>" + tag_address.item(i).getFirstChild().getNodeValue() + "</td>");
			out.println("<td width=100>" + tag_lat.item(i).getFirstChild().getNodeValue() + "</td>");
			out.println("<td width=100>" + tag_long.item(i).getFirstChild().getNodeValue() + "</td>");
		} catch(NullPointerException e) {
			out.println("<td width=100></td>");
		}		
		out.println("</tr>");
	}
%>

</body>
</html>